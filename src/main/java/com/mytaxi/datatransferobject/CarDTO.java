package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;


@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "licence plate not allowed")
    private String licensePlate;

    @NotNull(message = "seat count not allowed")
    private int seatCount;

    private int ratings;

    private EngineType engineType;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime manufactureYear;

    private CarType carType;

    private CarStatus carStatus;

    private String manufacturerName;

    private Boolean deleted;

    private DriverDO driver;

    private CarDTO(){}

    private CarDTO(CarDTOBuilder carDTOBuilder){
        this.id = carDTOBuilder.id;
        this.licensePlate = carDTOBuilder.licensePlate;
        this.carStatus = carDTOBuilder.carStatus;
        this.engineType = carDTOBuilder.engineType;
        this.seatCount = carDTOBuilder.seatCount;
        this.carType = carDTOBuilder.carType;
        this.manufactureYear = carDTOBuilder.manufactureYear;
        this.ratings = carDTOBuilder.ratings;
        this.manufacturerName = carDTOBuilder.manufacturerName;
        this.deleted = carDTOBuilder.deleted;
    }


    public static class CarDTOBuilder {

        private Long id;
        private String licensePlate;
        private int seatCount;
        private int ratings;
        private EngineType engineType;
        private ZonedDateTime manufactureYear;
        private CarType carType;
        private CarStatus carStatus;
        private String manufacturerName;
        private Boolean deleted;
        private DriverDO driver;

        public CarDTOBuilder setId(Long id){
            this.id = id;
            return this;
        }

        public CarDTOBuilder setLicencePlate(String licensePlate){
            this.licensePlate = licensePlate;
            return this;
        }

        public CarDTOBuilder setSeatCount(int seatCount){
            this.seatCount = seatCount;
            return this;
        }

        public CarDTOBuilder setRatings(int ratings){
            this.ratings = ratings;
            return this;
        }

        public CarDTOBuilder setEngineType(EngineType engineType){
            this.engineType = engineType;
            return this;
        }

        public CarDTOBuilder setManufacturerYear(ZonedDateTime manufacturerYear){
            this.manufactureYear = manufacturerYear;
            return this;
        }

        public CarDTOBuilder setCarType(CarType carType){
            this.carType = carType;
            return this;
        }

        public CarDTOBuilder setCarStatus(CarStatus carStatus){
            this.carStatus = carStatus;
            return this;
        }

        public CarDTOBuilder setManufacturerName(String manufacturerName){
            this.manufacturerName = manufacturerName;
            return this;
        }

        public CarDTOBuilder setDeleted(Boolean deleted){
            this.deleted = deleted;
            return this;
        }

        public CarDTOBuilder setDriver(DriverDO driver){
            this.driver = driver;
            return this;
        }

        public CarDTO builldCarDTO(){

            return new CarDTO(this);
        }

    }
}
