package com.mytaxi.security;

import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.DriverDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private DriverRepository driverRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        // Let people login with either username or email
        DriverDO user = driverRepository.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + username)
                );

        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {

        DriverDO user = driverRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("entity not found with id " + id)
        );

        return UserPrincipal.create(user);
    }


//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        DriverDO user = driverRepository.findByUsername(username)
//                .orElseThrow(() ->
//                        new UsernameNotFoundException("User not found with username or email : " + username)
//                );
//
//        List<GrantedAuthority> authorities = Lists.newArrayList();
//        authorities.add(new SimpleGrantedAuthority(user.getRole()));
//
//        UserDetails userDetails = new User(user.getUsername(), user.getPassword(), authorities);
//
//        return userDetails;
//    }
}
