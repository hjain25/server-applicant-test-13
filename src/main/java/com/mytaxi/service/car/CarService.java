package com.mytaxi.service.car;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;

public interface CarService {

    List<CarDTO> findCarsByStatus(CarStatus carStatus);

    CarDTO createCar(CarDTO carDTO);

    CarDTO update(CarDTO carDTO) throws EntityNotFoundException;

    void delete(Long carId) throws EntityNotFoundException;
}
