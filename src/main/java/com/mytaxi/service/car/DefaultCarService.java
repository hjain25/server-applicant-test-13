package com.mytaxi.service.car;

import com.mytaxi.controller.mapper.CarMapper;
import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class DefaultCarService implements CarService {

    private CarRepository carRepository;

    public DefaultCarService(final CarRepository carRepository){
        this.carRepository = carRepository;
    }


    /**
     * find cars by given status
     *
     * @param carStatus
     *
     */
    @Override
    public List<CarDTO> findCarsByStatus(CarStatus carStatus) {

        return carRepository.findByCarStatus(carStatus).
                stream().
                map(CarMapper::mapToCarDTO).
                collect(toList());
    }


    /**
     * register a new car
     *
     * @param carDTO
     *
     */
    @Override
    public CarDTO createCar(CarDTO carDTO) {

        CarDO newCar = carRepository.save(CarMapper.mapToCarDO(carDTO));
        return CarMapper.mapToCarDTO(newCar) ;
    }


    /**
     * update car attributes
     *
     * @param carDTO
     *
     */
    @Override
    public CarDTO update(CarDTO carDTO) throws EntityNotFoundException {

        CarDO carDO = carRepository.findById(carDTO.getId()).
                orElseThrow(() -> new EntityNotFoundException("car not found with id "+ carDTO.getId()));

        CarDO updatedCarDO = updateCar(carDO, carDTO);
        return CarMapper.mapToCarDTO(carRepository.save(updatedCarDO));
    }

    private static CarDO updateCar(CarDO carDO, CarDTO carDTO){

        carDO.setId(carDTO.getId());
        carDO.setLicensePlate(carDTO.getLicensePlate());
        carDO.setCarStatus(CarStatus.UNASSIGNED);
        carDO.setCarType(carDTO.getCarType());
        carDO.setEngineType(carDTO.getEngineType());
        carDO.setSeatCount(carDTO.getSeatCount());
        carDO.setRatings(carDTO.getRatings());
        carDO.setManufacturerName(carDTO.getManufacturerName());
        carDO.setManufactureYear(carDTO.getManufactureYear());

        return carDO;
    }


    /**
     * mark car as deleted
     *
     * @param carId
     *
     */
    @Override
    public void delete(Long carId) throws EntityNotFoundException {

        CarDO carDO = carRepository.findById(carId).
                orElseThrow(() -> new EntityNotFoundException("car not found with id " + carId));

        carDO.setDeleted(true);
        carRepository.save(carDO);
    }
}
