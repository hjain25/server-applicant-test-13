package com.mytaxi.service.driver;

import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.stream.Collectors.toList;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService {

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private DriverRepository driverRepository;

    private CarRepository carRepository;


    @Autowired
    public DefaultDriverService(final DriverRepository driverRepository, final CarRepository carRepository) {
        this.driverRepository = driverRepository;
        this.carRepository = carRepository;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException {
        return findDriverChecked(driverId);
    }


    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException {
        DriverDO driver;
        try {
            driver = driverRepository.save(driverDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to driver creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus) {
        return driverRepository.findByOnlineStatus(onlineStatus);
    }


    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException {
        DriverDO driverDO = driverRepository.findOne(driverId);
        if (driverDO == null) {
            throw new EntityNotFoundException("Could not find entity with id: " + driverId);
        }
        return driverDO;
    }


    /**
     * Assign car to driver
     *
     * @param driverId
     * @param carId
     */
    @Override
    public void assignCar(Long driverId, Long carId) throws CarAlreadyInUseException {

        CarDO carDO = carRepository.findOne(carId);

        if (carDO.getCarStatus() == CarStatus.ASSIGNED && !carDO.getDriver().getId().equals(driverId)) {
            throw new CarAlreadyInUseException();
        }

        DriverDO driverDO = driverRepository.findOne(driverId);
        driverDO.setCar(carDO);
        driverRepository.save(driverDO);

        carDO.setCarStatus(CarStatus.ASSIGNED);
        carDO.setDriver(driverDO);
        carRepository.save(carDO);
    }

    /**
     * de-link car to driver
     *
     * @param carId
     * @param driverId
     */
    @Override
    public void unAssignCar(Long driverId, Long carId) {

        CarDO carDO = carRepository.findOne(carId);
        carDO.setCarStatus(CarStatus.UNASSIGNED);
        carDO.setDriver(null);

        DriverDO driverDO = driverRepository.findOne(driverId);
        driverDO.setCar(null);
        driverRepository.save(driverDO);

        carRepository.save(carDO);
    }


    /**
     *  search drivers with given Specification type filter
     *
     * @param spec
     *
     */
    @Override
    public List<DriverDTO> findAll(Specification<DriverDO> spec) {
        return driverRepository.findAll(spec).stream().
                map(d -> DriverMapper.makeDriverDTO(d)).
                collect(toList());
    }

    /**
     *  find driver by given username
     *
     * @param username
     *
     */
    @Override
    public Optional<DriverDO> findByUsername(String username) throws EntityNotFoundException {

        return driverRepository.findByUsername(username);
    }
}
