package com.mytaxi.service.role;

import com.mytaxi.domainobject.Role;
import com.mytaxi.domainvalue.RoleName;

import java.util.Optional;

public interface RoleService {

    Optional<Role> findByName(RoleName roleUser);
}
