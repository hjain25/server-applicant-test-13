package com.mytaxi.service.role;

import com.mytaxi.dataaccessobject.RoleRepository;
import com.mytaxi.domainobject.Role;
import com.mytaxi.domainvalue.RoleName;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultRoleService implements RoleService {


    private RoleRepository roleRepository;

    public DefaultRoleService(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<Role> findByName(RoleName roleName) {

        return roleRepository.findByRoleName(roleName);
    }
}
