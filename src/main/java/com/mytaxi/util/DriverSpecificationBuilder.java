package com.mytaxi.util;

import com.google.common.collect.Lists;
import com.mytaxi.domainobject.DriverDO;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;

public class DriverSpecificationBuilder {

    private final List<SearchCriteria> params;

    public DriverSpecificationBuilder() {
        params = new ArrayList<SearchCriteria>();
    }

    public DriverSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<DriverDO> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<DriverDO>> specs = Lists.newArrayList();

        for (SearchCriteria param : params) {
            specs.add(new DriverSpecification(param));
        }

        Specification<DriverDO> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }
}
