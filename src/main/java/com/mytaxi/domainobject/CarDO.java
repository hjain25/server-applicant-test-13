package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;


@Setter
@Getter
@Entity
@Table(
        name = "car",
        uniqueConstraints = @UniqueConstraint(name = "uc_plateno", columnNames = {"licensePlate"})
)
public class CarDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false)
    @NotNull(message = "licensePlate can not be null")
    private String licensePlate;

    @Column(nullable = false)
    @NotNull(message = "seat count can not be null")
    private int seatCount;

    @Column
    private int ratings;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EngineType engineType;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime manufactureYear;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CarType carType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CarStatus carStatus;

    @Column(nullable = false)
    private Boolean deleted = false;

    @Column
    private String manufacturerName;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "car")
    private DriverDO driver;


    public CarDO(){}

    public CarDO(String licensePlate, EngineType engineType, CarType carType, int seatCount){

        this.licensePlate = licensePlate;
        this.engineType = engineType;
        this.carType = carType;
        this.carStatus = CarStatus.UNASSIGNED;
        this.seatCount = seatCount;
        this.deleted = false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarDO carDO = (CarDO) o;
        return seatCount == carDO.seatCount &&
                ratings == carDO.ratings &&
                Objects.equals(id, carDO.id) &&
                Objects.equals(dateCreated, carDO.dateCreated) &&
                Objects.equals(licensePlate, carDO.licensePlate) &&
                engineType == carDO.engineType &&
                Objects.equals(manufactureYear, carDO.manufactureYear) &&
                carType == carDO.carType &&
                carStatus == carDO.carStatus &&
                Objects.equals(deleted, carDO.deleted) &&
                Objects.equals(manufacturerName, carDO.manufacturerName) &&
                Objects.equals(driver, carDO.driver);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
