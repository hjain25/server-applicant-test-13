package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.RoleName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Setter
@Getter
@Entity
@Table(
        name = "role",
        uniqueConstraints = @UniqueConstraint(name = "uc_roleid", columnNames = {"id"})
)
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name="role_name")
    private RoleName roleName;

    @Column(name="description")
    private String description;
}
