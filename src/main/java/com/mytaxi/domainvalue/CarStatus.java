package com.mytaxi.domainvalue;

public enum CarStatus {

    ASSIGNED, UNASSIGNED
}
