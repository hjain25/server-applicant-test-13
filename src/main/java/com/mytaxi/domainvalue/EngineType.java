package com.mytaxi.domainvalue;

public enum EngineType {

    DIESEL, PETROL, CNG, ELECTRIC, HYBRID
}
