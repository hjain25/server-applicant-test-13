package com.mytaxi.controller;

import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.datatransferobject.DriverSignUpRequest;
import com.mytaxi.datatransferobject.JwtAuthenticationResponse;
import com.mytaxi.datatransferobject.LoginRequest;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.Role;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.domainvalue.RoleName;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.exception.MyTaxiException;
import com.mytaxi.security.JwtTokenProvider;
import com.mytaxi.service.role.RoleService;
import com.mytaxi.service.driver.DriverService;
import com.mytaxi.util.DriverSpecificationBuilder;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * All operations with a driver will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/drivers")
public class DriverController {

    private DriverService driverService;

    private DriverSpecificationBuilder driverSpecificationBuilder;

    private AuthenticationManager authenticationManager;

    private PasswordEncoder passwordEncoder;

    private JwtTokenProvider tokenProvider;

    private RoleService roleService;

    public DriverController(final DriverService driverService, final AuthenticationManager authenticationManager,
                            final PasswordEncoder passwordEncoder, final JwtTokenProvider tokenProvider,
                            final RoleService roleService) {
        this.driverService = driverService;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.roleService = roleService;
        this.driverSpecificationBuilder = new DriverSpecificationBuilder();
    }


    @GetMapping("/filter")
    public List<DriverDTO> searchDrivers(@RequestParam(value = "search") String search) {

        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
        Matcher matcher = pattern.matcher(search + ",");

        while (matcher.find()) {
            driverSpecificationBuilder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }

        Specification<DriverDO> spec = driverSpecificationBuilder.build();
        return driverService.findAll(spec);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerDriver(@Valid @RequestBody DriverSignUpRequest signUpRequest) throws EntityNotFoundException, ConstraintsViolationException, MyTaxiException {

        if(driverService.findByUsername(signUpRequest.getUsername()).isPresent()){

            return new ResponseEntity("username is already in use", HttpStatus.BAD_REQUEST);
        }

        // Creating user's account

        DriverDO driverDO = new DriverDO(signUpRequest.getUsername(), signUpRequest.getPassword());
        driverDO.setPassword(passwordEncoder.encode(driverDO.getPassword()));

        Role userRole = roleService.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new MyTaxiException("Driver Role not set."));
        driverDO.setRoles(Collections.singleton(userRole));

        DriverDO result = driverService.create(driverDO);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/v1/drivers/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body("Driver Registered successfully");
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateDriver(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @GetMapping("/{driverId}")
    @PreAuthorize("hasRole('USER')")
    public DriverDTO getDriver(@Valid @PathVariable long driverId) throws EntityNotFoundException {

        return DriverMapper.makeDriverDTO(driverService.find(driverId));
    }


    /*
        not removing this api for testing purposes
    */

    /*@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DriverDTO createDriver(@Valid @RequestBody DriverDTO driverDTO) throws ConstraintsViolationException {

        DriverDO driverDO = DriverMapper.makeDriverDO(driverDTO);
        return DriverMapper.makeDriverDTO(driverService.create(driverDO));
    }*/


    @DeleteMapping("/{driverId}")
    @PreAuthorize("hasRole('USER')")
    public void deleteDriver(@Valid @PathVariable long driverId) throws EntityNotFoundException {

        driverService.delete(driverId);
    }


    @PutMapping("/{driverId}")
    @PreAuthorize("hasRole('USER')")
    public void updateLocation(
            @Valid @PathVariable long driverId, @RequestParam double longitude, @RequestParam double latitude)
            throws ConstraintsViolationException, EntityNotFoundException {

        driverService.updateLocation(driverId, longitude, latitude);
    }

    @GetMapping
    public List<DriverDTO> findDrivers(@RequestParam OnlineStatus onlineStatus)
            throws ConstraintsViolationException, EntityNotFoundException {

        return DriverMapper.makeDriverDTOList(driverService.find(onlineStatus));
    }

    @PutMapping("/select/car")
    @PreAuthorize("hasRole('USER')")
    public void selectCar(@Valid @RequestParam("carId") Long carId, @Valid @RequestParam("driverId") Long driverId) throws CarAlreadyInUseException {

        driverService.assignCar(driverId, carId);
    }

    @PutMapping("/deselect/car")
    @PreAuthorize("hasRole('USER')")
    public void deselectCar(@Valid @RequestParam("carId") Long carId, @Valid @RequestParam("driverId") Long driverId) {

        driverService.unAssignCar(driverId, carId);
    }
}
