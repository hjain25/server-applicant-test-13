package com.mytaxi.controller;


import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.car.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/cars")
public class CarController {

    private CarService carService;

    public CarController(CarService carService){
        this.carService = carService;
    }


    @GetMapping
    public List<CarDTO> getAvailableCars(@RequestParam CarStatus carStatus){

        return carService.findCarsByStatus(carStatus);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CarDTO registerCar(@RequestBody CarDTO carDTO){

        return carService.createCar(carDTO);
    }

    @PutMapping
    public CarDTO updateCarProperties(@RequestBody CarDTO carDTO) throws EntityNotFoundException {

        return carService.update(carDTO);
    }

    @DeleteMapping("{/carId}")
    public void deleteCar(@Valid @PathVariable Long carId) throws EntityNotFoundException {

        carService.delete(carId);
    }

}

