package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.CarStatus;
import springfox.documentation.swagger2.mappers.ModelMapper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CarMapper {

    public static CarDTO mapToCarDTO(CarDO carDO){

        CarDTO carDTO = new CarDTO.CarDTOBuilder().setId(carDO.getId()).
                setCarStatus(carDO.getCarStatus()).
                setLicencePlate(carDO.getLicensePlate()).
                setEngineType(carDO.getEngineType()).
                setCarType(carDO.getCarType()).
                setSeatCount(carDO.getSeatCount()).
                setRatings(carDO.getRatings()).
                setDeleted(carDO.getDeleted()).
                setDriver(carDO.getDriver()).
                builldCarDTO();

        return carDTO;
    }

    public static CarDO mapToCarDO(CarDTO carDTO){

        CarDO carDO = new CarDO();
        carDO.setId(carDTO.getId());
        carDO.setLicensePlate(carDTO.getLicensePlate());
        carDO.setCarStatus(CarStatus.UNASSIGNED);
        carDO.setCarType(carDTO.getCarType());
        carDO.setEngineType(carDTO.getEngineType());
        carDO.setSeatCount(carDTO.getSeatCount());
        carDO.setRatings(carDTO.getRatings());
        carDO.setDeleted(carDTO.getDeleted());
        carDO.setManufacturerName(carDTO.getManufacturerName());

        return carDO;
    }


    public static ZonedDateTime formatToZonedDateTime(String time){

        String str = "2016-03-04 11:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        return ZonedDateTime.of(dateTime, ZoneId.of("IST"));
    }
}
