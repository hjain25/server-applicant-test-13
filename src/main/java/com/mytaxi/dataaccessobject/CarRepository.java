package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.CarStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Database Access Object for driver table.
 * <p/>
 */
@Repository
public interface CarRepository extends CrudRepository<CarDO, Long> {

    List<CarDO> findByCarStatus(CarStatus carStatus);

    Optional<CarDO> findById(Long id);
}
