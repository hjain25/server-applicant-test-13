package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.Role;
import com.mytaxi.domainvalue.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * repository for roles
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByRoleName(RoleName roleName);
}
