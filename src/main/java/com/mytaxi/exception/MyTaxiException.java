package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MyTaxiException extends Exception {

    public MyTaxiException(String message){
        super(message);
    }

    public MyTaxiException(String message, Throwable cause){
        super(message, cause);
    }
}
