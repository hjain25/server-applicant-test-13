/**
 * CREATE Script for init of DB
 */

-- Create 3 OFFLINE drivers

-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (1, now(), false, 'OFFLINE',
-- 'driver01pw', 'driver01', 'ROLE_USER');
--
-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (2, now(), false, 'OFFLINE',
-- 'driver02pw', 'driver02', 'ROLE_USER');
--
-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (3, now(), false, 'OFFLINE',
-- 'driver03pw', 'driver03', 'ROLE_USER');
--
--
-- -- Create 3 ONLINE drivers
--
-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (4, now(), false, 'ONLINE',
-- 'driver04pw', 'driver04', 'ROLE_USER');
--
-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (5, now(), false, 'ONLINE',
-- 'driver05pw', 'driver05', 'ROLE_USER');
--
-- insert into driver (id, date_created, deleted, online_status, password, username, role) values (6, now(), false, 'ONLINE',
-- 'driver06pw', 'driver06', 'ROLE_USER');
--
-- -- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)
--
-- insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username, role)
-- values
--  (7,
--  'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
-- '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'driver07', 'ROLE_USER');
--
-- -- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)
--
-- insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username, role)
-- values
--  (8,
--  'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
-- 'driver08pw', 'driver08', 'ROLE_USER');



insert into car (id, license_plate, car_status, car_type, date_created, engine_type, manufacture_year, deleted, ratings, seat_count)
values
 (11,
 'abcd1234', 'UNASSIGNED', 'SUV', now(), 'ELECTRIC', now(), false, 4, 6);
 insert into car (id, license_plate, car_status, car_type, date_created, engine_type, manufacture_year, deleted, ratings, seat_count)
values
 (12,
 'efgh1234', 'UNASSIGNED', 'HATCHBACK', now(), 'HYBRID', now(), false, 3, 4);
 insert into car (id, license_plate, car_status, car_type, date_created, engine_type, manufacture_year, deleted, ratings, seat_count)
values
 (13,
 'ijkl1234', 'UNASSIGNED', 'SEDAAN', now(), 'CNG', now(), false, 4, 3);



INSERT INTO role (id, role_name, description) VALUES (1, 'ROLE_USER', 'Standard User - Has no admin rights');

