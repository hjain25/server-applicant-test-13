package com.mytaxi.factory;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import org.assertj.core.util.Lists;

import java.util.List;

public class CarObjectFactory {

    private CarObjectFactory(){}

    public static List<CarDO> createDummyCarDOs(){

        CarDO car1 = new CarDO("abcd1234", EngineType.CNG, CarType.HATCHBACK, 4);
        car1.setId(Long.valueOf(01));
        car1.setManufacturerName("bmw");


        CarDO car2 = new CarDO("abcd5678", EngineType.HYBRID, CarType.SUV, 6);
        car2.setId(Long.valueOf(02));
        car2.setManufacturerName("bolero");

        return Lists.newArrayList(car1, car2);
    }

    public static CarDTO createDummyCarDTO() {

        CarDO carDO = createDummyCarDOs().get(0);

        CarDTO carDTO = new CarDTO.CarDTOBuilder().setId(carDO.getId()).
                setCarStatus(carDO.getCarStatus()).
                setLicencePlate(carDO.getLicensePlate()).
                setEngineType(carDO.getEngineType()).
                setCarType(carDO.getCarType()).
                setSeatCount(carDO.getSeatCount()).
                setRatings(carDO.getRatings()).
                setDeleted(carDO.getDeleted()).
                setDriver(carDO.getDriver()).
                builldCarDTO();

        return carDTO;
    }
}
