package com.mytaxi.factory;

import com.mytaxi.domainobject.DriverDO;

public class DriverObjectFactory {

    public final static String DRIVER_NAME = "driver01";

    public final static String DRIVER_PASSWORD = "pass01";

    private DriverObjectFactory(){}


    public static DriverDO createDummyDriverDO() {

        DriverDO driverDO = new DriverDO(DRIVER_NAME, DRIVER_PASSWORD);
        driverDO.setId(Long.valueOf(01));
        return driverDO;
    }
}
