package com.mytaxi.service.car;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.factory.CarObjectFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @Mock
    private CarRepository carRepository;

    @InjectMocks
    private CarService carService = new DefaultCarService(carRepository);

    @Captor
    private ArgumentCaptor<CarDO> carDOCaptor = ArgumentCaptor.forClass(CarDO.class);

    @Test
    public void givenCarDetails_ThenCreateNewCar() {

        // GIVEN
        CarDTO dummyCarDTO = CarObjectFactory.createDummyCarDTO();
        CarDO dummCarDO = CarObjectFactory.createDummyCarDOs().get(0);

        // WHEN
        when(carRepository.save(carDOCaptor.capture())).thenReturn(dummCarDO);

        // THEN
        CarDTO actualCarDTO = carService.createCar(dummyCarDTO);
        assertThat(actualCarDTO).isEqualToComparingFieldByField(dummyCarDTO);

        // VERIFY
        verify(carRepository).save(any(CarDO.class));
    }

    @Test
    public void givenCarId_WhenCarIsFound_ThenMarkCarAsDeleted() throws EntityNotFoundException {

        // GIVEN
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);

        // WHEN
        when(carRepository.findById(anyLong())).thenReturn(Optional.of(dummyCarDO));
        when(carRepository.save(any(CarDO.class))).thenReturn(dummyCarDO);

        // THEN
        carService.delete(dummyCarDO.getId());

        // VERIFY
        verify(carRepository).findById(dummyCarDO.getId());
        verify(carRepository).save(carDOCaptor.capture());

        assertThat(carDOCaptor.getValue().getDeleted()).isEqualTo(true);
    }

    @Test
    public void givenCarId_WhenCarNotFound_ThenThrowEntityNotFoundException() throws EntityNotFoundException {

        // GIVEN
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);

        // WHEN
        when(carRepository.findById(anyLong())).thenReturn(Optional.empty());

        // THEN
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() ->
                carService.delete(dummyCarDO.getId()));

        // VERIFY
        verify(carRepository).findById(dummyCarDO.getId());
        verify(carRepository, never()).save(any(CarDO.class));
    }

    @Test
    public void givenCarId_WhenCarIsFound_ThenUpdateCarProperties() throws EntityNotFoundException {

        // GIVEN
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);
        CarDTO dummyCarDTO = CarObjectFactory.createDummyCarDTO();
        dummyCarDO.setEngineType(EngineType.HYBRID);
        dummyCarDO.setSeatCount(5);

        // WHEN
        when(carRepository.findById(anyLong())).thenReturn(Optional.of(dummyCarDO));
        when(carRepository.save(any(CarDO.class))).thenReturn(dummyCarDO);

        // THEN
        CarDTO actualCarDTO = carService.update(dummyCarDTO);
        assertThat(actualCarDTO).isEqualToComparingFieldByField(dummyCarDTO);

        // VERIFY
        verify(carRepository).findById(dummyCarDO.getId());
        verify(carRepository).save(any(CarDO.class));
    }

    @Test
    public void givenCarId_WhenCarIsNotFound_ThenThrowEntityNotFoundException() throws EntityNotFoundException {

        // GIVEN
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);
        CarDTO dummyCarDTO = CarObjectFactory.createDummyCarDTO();
        dummyCarDO.setEngineType(EngineType.HYBRID);
        dummyCarDO.setSeatCount(5);

        // WHEN
        when(carRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(carRepository.save(any(CarDO.class))).thenReturn(dummyCarDO);

        // THEN
        assertThatExceptionOfType(EntityNotFoundException.class).isThrownBy(() ->
                carService.update(dummyCarDTO));

        // VERIFY
        verify(carRepository).findById(dummyCarDO.getId());
        verify(carRepository, never()).save(any(CarDO.class));
    }

    @Test
    public void givenCarStatus_ThenFindAllCarsByStatus(){

        // GIVEN
        List<CarDO> carDOs = CarObjectFactory.createDummyCarDOs();

        // WHEN
        when(carRepository.findByCarStatus(CarStatus.UNASSIGNED)).thenReturn(carDOs);

        // THEN
        List<CarDO> actualCarDOs = carRepository.findByCarStatus(CarStatus.UNASSIGNED);

        actualCarDOs.forEach(c -> assertThat(c.getCarStatus()).isEqualTo(CarStatus.UNASSIGNED));

        // VERIFY
        verify(carRepository).findByCarStatus(CarStatus.UNASSIGNED);
    }
}
