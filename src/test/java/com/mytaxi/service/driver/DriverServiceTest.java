package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.CarStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.factory.CarObjectFactory;
import com.mytaxi.factory.DriverObjectFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DriverServiceTest {

    @Mock
    private DriverRepository driverRepository;

    @Mock
    private CarRepository carRepository;


    @InjectMocks
    private DriverService driverService = new DefaultDriverService(driverRepository, carRepository);

    @Captor
    private ArgumentCaptor<CarDO> carDOCaptor = ArgumentCaptor.forClass(CarDO.class);

    @Captor
    private ArgumentCaptor<DriverDO> driverDOCaptor = ArgumentCaptor.forClass(DriverDO.class);

    @Test
    public void givenDriverIdAndCarId_ThenAssignCarToDriver() throws CarAlreadyInUseException {

        // GIVEN
        DriverDO dummyDriverDO = DriverObjectFactory.createDummyDriverDO();
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);

        // WHEN
        when(driverRepository.findOne(anyLong())).thenReturn(dummyDriverDO);
        when(carRepository.findOne(anyLong())).thenReturn(dummyCarDO);
        when(carRepository.save(carDOCaptor.capture())).thenReturn(dummyCarDO);
        when(driverRepository.save(driverDOCaptor.capture())).thenReturn(dummyDriverDO);

        // THEN
        driverService.assignCar(Long.valueOf(01), Long.valueOf(01) );

        // VERIFY
        verify(driverRepository).findOne(dummyDriverDO.getId());
        verify(carRepository).findOne(dummyCarDO.getId());
        verify(driverRepository).save(any(DriverDO.class));
        verify(carRepository).save(any(CarDO.class));

        assertThat(carDOCaptor.getValue().getCarStatus()).isEqualTo(CarStatus.ASSIGNED);
        assertThat(carDOCaptor.getValue().getDriver()).isEqualTo(dummyDriverDO);

        assertThat(driverDOCaptor.getValue().getCar()).isEqualToComparingFieldByField(dummyCarDO);
    }

    @Test
    public void givenDriverIdAndCarId_ShouldThrowCarAlreadyInUseException() throws CarAlreadyInUseException {

        // GIVEN
        DriverDO dummyDriverDO = DriverObjectFactory.createDummyDriverDO();

        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);
        dummyCarDO.setDriver(dummyDriverDO);
        dummyCarDO.setCarStatus(CarStatus.ASSIGNED);

        // WHEN
        when(carRepository.findOne(anyLong())).thenReturn(dummyCarDO);

        // THEN
        assertThatExceptionOfType(CarAlreadyInUseException.class)
                .isThrownBy(() ->  driverService.assignCar(Long.valueOf(02), Long.valueOf(01)));

        // VERIFY
        verify(carRepository).findOne(dummyCarDO.getId());
        verify(driverRepository, never()).findOne(anyLong());
        verify(driverRepository, never()).save(any(DriverDO.class));
        verify(carRepository, never()).save(any(CarDO.class));
    }

    @Test
    public void givenDriverIdAndCarId_ThenUnAssignCarToDriver() throws CarAlreadyInUseException {

        // GIVEN
        DriverDO dummyDriverDO = DriverObjectFactory.createDummyDriverDO();
        CarDO dummyCarDO = CarObjectFactory.createDummyCarDOs().get(0);

        dummyDriverDO.setCar(dummyCarDO);
        dummyCarDO.setDriver(dummyDriverDO);

        // WHEN
        when(driverRepository.findOne(anyLong())).thenReturn(dummyDriverDO);
        when(carRepository.findOne(anyLong())).thenReturn(dummyCarDO);
        when(carRepository.save(carDOCaptor.capture())).thenReturn(dummyCarDO);
        when(driverRepository.save(driverDOCaptor.capture())).thenReturn(dummyDriverDO);

        // THEN
        driverService.unAssignCar(Long.valueOf(01), Long.valueOf(01));

        // VERIFY
        verify(driverRepository).findOne(dummyDriverDO.getId());
        verify(carRepository).findOne(dummyCarDO.getId());
        verify(driverRepository).save(any(DriverDO.class));
        verify(carRepository).save(any(CarDO.class));

        assertThat(carDOCaptor.getValue().getCarStatus()).isEqualTo(CarStatus.UNASSIGNED);
        assertThat(carDOCaptor.getValue().getDriver()).isNull();

        assertThat(driverDOCaptor.getValue().getCar()).isNull();
    }
}
